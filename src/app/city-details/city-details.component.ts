import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiHelperService } from '../api-helper.service';

@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: ['./city-details.component.scss'],
})
export class CityDetailsComponent implements OnInit {
  public forecastData = [];
  constructor(
    public route: ActivatedRoute,
    public apiHelperService: ApiHelperService
  ) {}

  ngOnInit(): void {
    const cityName = this.route.snapshot.paramMap.get('name');
    this.getForecastData(cityName);
  }

  public getForecastData(cityName: string): void {
    this.apiHelperService.getForecastData(cityName).subscribe((data) => {
      this.forecastData = data.list.filter((value, i: number) => {
        return !(i % 8);
      });
      this.forecastData = this.forecastData.map((value) => {
        const forecastVal = {
          date: value.dt_txt,
          ...value.main,
        };
        return forecastVal;
      });
    });
  }
}
