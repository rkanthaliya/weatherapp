import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';

const API_KEY = '56904bb277a504a934ae10f129248536';

@Injectable({
  providedIn: 'root',
})
export class ApiHelperService {
  constructor(private http: HttpClient) {}

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  public baseURL = 'http://api.openweathermap.org/data/2.5';

  public getbasicData(city: string): Observable<any> {
    const endURL = `${this.baseURL}/weather?APPID=${API_KEY}&q=${city}`;
    return this.http.get(endURL).pipe(retry(1), catchError(this.handleError));
  }

  public getForecastData(city: string): Observable<any> {
    const endURL = `${this.baseURL}/forecast?APPID=${API_KEY}&q=${city}`;
    return this.http.get(endURL).pipe(retry(1), catchError(this.handleError));
  }

  // Error handling
  handleError(error: HttpErrorResponse): Observable<never> {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
