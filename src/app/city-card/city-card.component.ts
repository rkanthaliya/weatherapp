import { Component, Input, OnInit } from '@angular/core';
import { ApiHelperService } from '../api-helper.service';

export interface ICity {
  name: string;
  pictureSrc: string;
  temp?: string;
  sunrise?: number;
  sunset?: number;
}

@Component({
  selector: 'app-city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.scss'],
})
export class CityCardComponent implements OnInit {
  @Input() city: ICity;

  constructor(public apiHelperService: ApiHelperService) {}

  ngOnInit(): void {
    this.apiHelperService.getbasicData(this.city.name).subscribe((data) => {
      const cityDetails = {
        temp: (data.main.temp - 273.15).toFixed(2),
        sunrise: data.sys.sunrise,
        sunset: data.sys.sunset,
      };
      this.city = { ...this.city, ...cityDetails };
    });
  }
}
