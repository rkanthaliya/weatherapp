import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CityCardComponent } from './city-card/city-card.component';
import { HttpClientModule } from '@angular/common/http';
import { CityDetailsComponent } from './city-details/city-details.component';

@NgModule({
  declarations: [AppComponent, HomeComponent, CityCardComponent, CityDetailsComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
